<?
// List directories in /photos as links.
// Foreach
//  parse images in directory and display.
//  get captions from captions.txt based on image name, and display with image.
//  offer zip to download

$dirs = array();
foreach(scandir("photos") as $d){
    if(is_dir("photos/".$d)){
        $dirs[] = $d;
    }
}
$photos = array();
if(isset($_GET["dir"])){
    $dir = urldecode($_GET["dir"]);
    $photos = glob("photos/".$dir."/*");
}
?>
<!doctype html>
<html>
<head>
    <title>/photos</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
    <h1>/photos</h1>
    <?=isset($_GET["dir"]) ? "<h2>".$dir."</h2><p>".count($photos)." photos</p>" : ""?>
    <?foreach($photos as $photo):?>
        <p><img src="<?=$photo?>" title="<?=$photo?>" /></p>
    <?endforeach?>
    <ul>
    <?for($i=2;$i<count($dirs);$i++):?>
        <li><a href="?dir=<?=urlencode($dirs[$i])?>"><?=$dirs[$i]?></a></li>
    <?endfor?>
    </ul>
</body>
</html>